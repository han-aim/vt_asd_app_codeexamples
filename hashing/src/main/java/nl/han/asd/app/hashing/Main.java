package nl.han.asd.app.hashing;

public class Main {

    private static int[] array = new int[11];
    ;

    private static int hash(int key) {
        return key * 7;
    }

    private static void put(int value) {
        int index = hash(value) % 11;
        System.out.println("Inserting " + value + " at " + index);
        array[index] = value;
    }

    public static void main(String[] args) {
        // 2, 4, 5, 1, 8, 14, 12.
        put(2);
        put(4);
        put(5);
        put(1);
        put(8);
        put(14);
        put(12);
    }
}
