package nl.han.asd.app.expressions;

import nl.han.asd.app.expressions.ast.ASTNode;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

public class ParserPipeline {
    public ASTNode parse(CharStream input) {
        final ExpressionLexer lexer = new ExpressionLexer(input);
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final ExpressionParser expressionParser = new ExpressionParser(tokens);
        final ParseTree cst = expressionParser.root();
        final ParseTreeWalker cstWalker = new ParseTreeWalker();
        final ParserCustom parserCustom = new ParserCustom();
        cstWalker.walk(parserCustom, cst);
        return parserCustom.getAst();
    }
}
