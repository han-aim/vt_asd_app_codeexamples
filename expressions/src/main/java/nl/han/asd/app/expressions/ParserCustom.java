package nl.han.asd.app.expressions;

import nl.han.asd.app.expressions.ast.ASTNode;

import java.util.Stack;

class ParserCustom extends ExpressionBaseListener {
    private final Stack<ASTNode> stackAst = new Stack<>();
    private ASTNode ast = null;

    final ASTNode getAst() {
        return ast;
    }

    @Override
    public void exitReal(final ExpressionParser.RealContext ctx) {
        super.exitReal(ctx);
        final int indexOfNumber = ctx.children.size() - 1;
        double value = Double.parseDouble(ctx.getChild(indexOfNumber).getText());
        final int numberOfUnaryOperators = indexOfNumber;
        final long numberOfMinusSigns = ctx.children.stream()
                .filter(child -> child.getText().equals("-"))
                .count();
        final int balance = numberOfUnaryOperators / 2;
        // Semantics chosen: A number is positive by default. If more than half of the unary operators are minus signs, the number is negative.
        boolean isNegativeNumber = numberOfMinusSigns > 0 && numberOfMinusSigns > balance;
        if (isNegativeNumber) {
            value = value * -1;
        }
        stackAst.push(new ASTNode.Real(value));
    }

    @Override
    public void exitRoot(ExpressionParser.RootContext ctx) {
        super.exitRoot(ctx);
        this.ast = stackAst.pop();
    }

    @Override
    public void exitExpression(final ExpressionParser.ExpressionContext ctx) {
        super.exitExpression(ctx);
        // Check if expression with operator (i.e., non-terminal).
        if (ctx.operator != null) {
            // Get middle child, labeled 'operator' in the grammar.
            final ASTNode right = stackAst.pop();
            final ASTNode left = stackAst.pop();
            final String operator = ctx.operator.getText();
            final ASTNode astNode = switch (operator) {
                case "*" -> new ASTNode.Multiply(left, right);
                case "-" -> new ASTNode.Subtract(left, right);
                case "+" -> new ASTNode.Sum(left, right);
                case "/" -> new ASTNode.Divide(left, right);
                // We do not need our checker to check for other operators, since these aren't allowed by the grammar, so such input would be syntactically invalid. However, Java is unable to infer this.
                default -> throw new IllegalStateException("Unexpected value: " + operator);
            };
            stackAst.push(astNode);
        }
    }
}
