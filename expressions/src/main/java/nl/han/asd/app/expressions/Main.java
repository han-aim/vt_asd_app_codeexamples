package nl.han.asd.app.expressions;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        args = null;
        final CharStream charStream = CharStreams.fromStream(System.in);
        final ParserPipeline parserPipeline = new ParserPipeline();
        parserPipeline.parse(charStream);
    }
}
