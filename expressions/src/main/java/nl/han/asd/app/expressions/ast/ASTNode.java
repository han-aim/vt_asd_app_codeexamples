package nl.han.asd.app.expressions.ast;

public abstract class ASTNode {
    ASTNode childLeft = null;
    ASTNode childRight = null;

    private ASTNode(ASTNode childLeft, ASTNode childRight) {
        this.childLeft = childLeft;
        this.childRight = childRight;
    }

    private ASTNode() {
    }

    @Override
    public String toString() {
        return "\nchildLeft = " + this.childLeft +
                ",\nchildRight = " + this.childRight;
    }

    public abstract double evaluate();

    public static final class Real extends ASTNode {
        private double value;

        public Real(double value) {
            super();
            this.value = value;
        }

        public final double evaluate() {
            return this.value;
        }

        @Override
        public final String toString() {
            return "Real { value = " + this.value + " }";
        }
    }

    public static final class Sum extends ASTNode {
        public Sum(ASTNode childLeft, ASTNode childRight) {
            super(childLeft, childRight);
        }

        @Override
        public final String toString() {
            return "Sum {" +
                    super.toString() + '}';
        }

        @Override
        public final double evaluate() {
            if (this.childRight instanceof Multiply || this.childRight instanceof Divide) {
                return this.childRight.evaluate() + this.childLeft.evaluate();
            } else {
                return this.childLeft.evaluate() + this.childRight.evaluate();
            }
        }
    }

    public static final class Subtract extends ASTNode {
        public Subtract(ASTNode childLeft, ASTNode childRight) {
            super(childLeft, childRight);
        }

        @Override
        public final String toString() {
            return "Subtract {" +
                    super.toString() + '}';
        }

        @Override
        public final double evaluate() {
            if (this.childRight instanceof Multiply || this.childRight instanceof Divide) {
                return this.childRight.evaluate() - this.childLeft.evaluate();
            } else {
                return this.childLeft.evaluate() - this.childRight.evaluate();
            }
        }
    }

    public static final class Multiply extends ASTNode {
        public Multiply(ASTNode childLeft, ASTNode childRight) {
            super(childLeft, childRight);
        }

        @Override
        public final String toString() {
            return "Multiply {" +
                    super.toString() + '}';
        }

        @Override
        public final double evaluate() {
            if (this.childRight instanceof Multiply || this.childRight instanceof Divide) {
                return this.childRight.evaluate() * this.childLeft.evaluate();
            } else {
                return this.childLeft.evaluate() * this.childRight.evaluate();
            }
        }
    }

    public static final class Divide extends ASTNode {
        public Divide(ASTNode childLeft, ASTNode childRight) {
            super(childLeft, childRight);
        }

        @Override
        public final String toString() {
            return "Divide {" +
                    super.toString() + '}';
        }

        @Override
        public final double evaluate() {
            if (this.childRight instanceof Multiply || this.childRight instanceof Divide) {
                return this.childRight.evaluate() / this.childLeft.evaluate();
            } else {
                return this.childLeft.evaluate() / this.childRight.evaluate();
            }
        }
    }
}
