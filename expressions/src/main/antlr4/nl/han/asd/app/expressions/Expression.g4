grammar Expression;

root : expression EOF;
real : OPERATOR_UNARY_OR_BINARY* INT (DECIMALSEPARATOR INT)? (EXP OPERATOR_UNARY_OR_BINARY* INT)?;
expression : real | expression operator = OPERATOR_BINARY expression | expression operator = OPERATOR_UNARY_OR_BINARY expression;

WS : [ \t\r\n]+ -> skip;
// Numbers may not start with zero, if not equal to zero.
INT: '0' | [1-9][0-9]*;
EXP: [eE];
// Combining all operators in one token, rather than listing them all in expression alternatives, is needed because otherwise the order of listing influences the CST, which would no longer represent left-associativity.
OPERATOR_BINARY: OPERATOR_DIVIDE | OPERATOR_MULTIPLY;
// '+' and '-' can be unary operators too, e.g. '-1' and '1*-1' have a semantics, while '*1' and '1**-1' do not.
OPERATOR_UNARY_OR_BINARY: OPERATOR_SUBTRACT | OPERATOR_SUM;
OPERATOR_DIVIDE: '/';
OPERATOR_MULTIPLY: '*';
OPERATOR_SUM: '+';
OPERATOR_SUBTRACT: '-';
DECIMALSEPARATOR: '.';
