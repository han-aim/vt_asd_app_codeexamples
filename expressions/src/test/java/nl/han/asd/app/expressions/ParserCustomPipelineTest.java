package nl.han.asd.app.expressions;

import nl.han.asd.app.expressions.ast.ASTNode;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParserCustomPipelineTest {
    private ParserPipeline parserPipeline = null;

    @BeforeEach
    void setUp() {
        parserPipeline = new ParserPipeline();
    }

    @Test
    void basic() {
        final CharStream charStream = CharStreams.fromString("12 * 4 - 3 + -45");
        final ASTNode ast = parserPipeline.parse(charStream);
        assertEquals(ast.toString(), """
            Sum {
            childLeft = Subtract {
            childLeft = Multiply {
            childLeft = Real { value = 12.0 },
            childRight = Real { value = 4.0 }},
            childRight = Real { value = 3.0 }},
            childRight = Real { value = -45.0 }}""");
        assertEquals(0, ast.evaluate());
    }

    @Test
    void repeatedUnaryMinus() {
        final CharStream charStream = CharStreams.fromString("12 * 4 - 3 + ---45");
        final ASTNode ast = parserPipeline.parse(charStream);
        assertEquals("""
            Sum {
            childLeft = Subtract {
            childLeft = Multiply {
            childLeft = Real { value = 12.0 },
            childRight = Real { value = 4.0 }},
            childRight = Real { value = 3.0 }},
            childRight = Real { value = -45.0 }}""",
                ast.toString());
        assertEquals(0, ast.evaluate());
    }

    @Test
    void unaryPlusMinus() {
        final CharStream charStream = CharStreams.fromString("12 * 4 - 3 + +-45");
        final ASTNode ast = parserPipeline.parse(charStream);
        assertEquals("""
            Sum {
            childLeft = Subtract {
            childLeft = Multiply {
            childLeft = Real { value = 12.0 },
            childRight = Real { value = 4.0 }},
            childRight = Real { value = 3.0 }},
            childRight = Real { value = 45.0 }}""",
                ast.toString());
        assertEquals(90, ast.evaluate());
    }
}
